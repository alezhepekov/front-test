import { Component, OnInit } from '@angular/core';

import { ConnectionService } from './connection.service';
import { IpInfoService } from './ipinfo.service';
import { Connection } from './connection';

declare var window: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  connections: any = new Array();

  constructor(
    private connectionService: ConnectionService,
    private ipInfoService: IpInfoService
  ) { }

  ngOnInit() {
    this.getConnections();
  }

  getConnections() {
    this.connectionService.getConnections().subscribe(data => {
      for (const key in data) {
        if (data.hasOwnProperty(key)) {
          const ip = data[key][0];
          const count = data[key][1];
          this.ipInfoService.getIPInfo(ip).subscribe(ipInfo => {
            const newConnection = new Connection();
            newConnection.ipInfo = Object.assign({}, ipInfo);
            newConnection.ip = ip;
            newConnection.count = count;
            this.connections.push(newConnection);

            window.addMarker(newConnection);
          });
        }
      }
    },
    err => {
      console.error('error: ', err);
    });
  }
}
