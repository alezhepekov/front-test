import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {

  private serviceUrl = 'assets/json/sample.json';

  constructor(
    private http: HttpClient
  ) {}

  getConnections (): Observable<object> {
    return this.http.get<object>(this.serviceUrl)
      .pipe(
        catchError(this.handleError('getConnections'))
      );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(`ConnectionService: ${message}`);
  }
}
