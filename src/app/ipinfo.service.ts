import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap, retry } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class IpInfoService {

  private serviceUrl = 'http://geoip.nekudo.com/api/';

  constructor(
    private http: HttpClient
  ) {}

  getIPInfo(ip: string): Observable<object> {
    return this.http.get<object>(`${this.serviceUrl}/${ip}`)
      .pipe(
        retry(5),
        catchError(this.handleError('getIPInfo'))
      );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(`IpInfoService: ${message}`);
  }
}
