ymaps.ready(init);

function init() {
  var myMap = new ymaps.Map("map", {
    center: [55.76, 37.64],
    zoom: 3
  }, {
    searchControlProvider: 'yandex#search'       
  });

  window.myMap = myMap;
}

function addMarker(data) {
  if (window.myMap && data && data.ipInfo && data.ipInfo.location && data.ipInfo.location.latitude && data.ipInfo.location.longitude && data.ip && data.count) {
    window.myMap.geoObjects
      .add(new ymaps.Placemark([data.ipInfo.location.latitude, data.ipInfo.location.longitude], {
        balloonContent: '<span>Ip: <b>' + data.ip + '</b><br>Кол-во подключений: <b>' + data.count + '</b></span>'
      }, {
        preset: 'islands#icon',
        iconColor: '#0095b6'
      }))
  }
}
window.addMarker = addMarker;